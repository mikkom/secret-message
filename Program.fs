﻿open System
open System.IO
open SixLabors.ImageSharp
open SixLabors.ImageSharp.PixelFormats

type PixelType = DrawUp | DrawLeft | Stop | TurnRight | TurnLeft | Continue

let (|RGB|) (color: Rgba32) = (int color.R, int color.G, int color.B)

let toPixelType = function
    | RGB(7, 84, 19) -> DrawUp
    | RGB(139, 57, 137) -> DrawLeft
    | RGB(51, 69, 169) -> Stop
    | RGB(182, 149, 72) -> TurnRight
    | RGB(123, 131, 154) -> TurnLeft
    | _ -> Continue

type Direction = Up = 0 | Right = 1 | Down = 2 | Left = 3

let turnRight (dir: Direction) = enum<Direction>((int dir + 1) % 4)
// Two wrongs don't make a right but three rights make a left
let turnLeft (dir: Direction) = enum<Direction>((int dir + 3) % 4)

let getDirection currentDirection = function
    | DrawUp -> Some Direction.Up
    | DrawLeft -> Some Direction.Left
    | TurnRight -> Some (turnRight currentDirection)
    | TurnLeft -> Some (turnLeft currentDirection)
    | Continue -> Some currentDirection
    | Stop -> None

let assuming isValid value = if isValid then (Some value) else None

let getPath (width, height) (input: PixelType[]) idx startPixelType =
    let move (x, y) = function
        | Direction.Up -> assuming (y > 0) (x, y - 1)
        | Direction.Left -> assuming (x > 0) (x - 1, y)
        | Direction.Right -> assuming (x < width - 1) (x + 1, y)
        | Direction.Down -> assuming (y < height - 1) (x, y + 1)
        | dir -> failwithf "Invalid direction %A" dir

    let rec loop acc (x, y) dir =
        let idx = y * width + x
        let pixelType = input.[idx]
        let dir = getDirection dir pixelType

        if Set.contains ((x, y), dir) acc
        then acc // We have already moved to same direction from this pixel
        else
            let acc = Set.add ((x, y), dir) acc
            match Option.bind (move (x, y)) dir with
                | Some (x, y) -> loop acc (x, y) dir.Value
                | _ -> acc

    let startDir =
        match startPixelType with
        | DrawUp -> Direction.Up
        | DrawLeft -> Direction.Left
        | _ -> failwithf "Invalid start pixel type %A" startPixelType

    let (x, y) = (idx % width, idx / width)
    loop Set.empty (x, y) startDir |> Set.toArray |> Array.map fst

let getPixels size input =
    input
    |> Array.Parallel.mapi (fun i t -> i, t)
    |> Array.filter (fun (_, t) -> t = DrawUp || t = DrawLeft)
    |> Array.Parallel.collect (fun (i, t) -> getPath size input i t)

let processImage size (inputImg: Image<Rgba32>) (outputImg: Image<Rgba32>) =
    let (width, height) = size
    let getPixel i = inputImg.[i % width, i / width]
    let input = Array.Parallel.init (width * height) (getPixel >> toPixelType)
    let output = getPixels size input
    // Set the pixel colors in output image
    Array.Parallel.iter (fun (x, y) -> outputImg.[x, y] <- Rgba32.Red) output

[<EntryPoint>]
let main argv =
    let stopWatch = System.Diagnostics.Stopwatch.StartNew()
    let (inputFileName, outputFileName) =
        match argv.Length with
        | 0 -> ("img/input.png", "output.png")
        | 1 -> (argv.[0], "output.png")
        | _ -> (argv.[0], argv.[1])

    use stream = File.OpenRead(inputFileName)
    let input = Image.Load(stream)
    let size = (input.Width, input.Height)
    use output = new Image<Rgba32>(input.Width, input.Height)
    printfn "Image `%s` read in %f ms"
        inputFileName stopWatch.Elapsed.TotalMilliseconds
    stopWatch.Restart()

    processImage size input output
    printfn "Processing done in %f ms" stopWatch.Elapsed.TotalMilliseconds
    stopWatch.Restart()

    output.Save(outputFileName) |> ignore
    stopWatch.Stop()
    printfn "Output `%s` written to disk in %f ms"
        outputFileName stopWatch.Elapsed.TotalMilliseconds

    0 // return an integer exit code
