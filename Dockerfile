FROM microsoft/dotnet:2.2-sdk
WORKDIR /secret-message

# copy project.json & nuget.config and restore as distinct layers
COPY secret-message.fsproj .
RUN dotnet restore

# copy and build everything else
COPY . .
RUN dotnet publish -c Release -o out
ENTRYPOINT ["dotnet", "out/secret-message.dll"]
